#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"

class SpaceShip : public SceneObject
{
	public:
		SpaceShip();
		~SpaceShip();

  protected:
    void privateInit();
		void privateRender();
		void privateUpdate();

	private:
	  float speed_;
    float life_;
    float armor_;
    
};

