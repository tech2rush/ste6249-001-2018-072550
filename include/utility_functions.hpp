#include <iostream>

namespace shooter {

void printGLErrors(std::string s, bool all)
{
    GLenum err = glGetError();
    if (all) while ( err != GL_NO_ERROR ) std::cout << "OpenGL (" << s << ") error: " << err << " " << gluErrorString(err) << std::endl;
    else if( err != GL_NO_ERROR ) std::cout << "OpenGL (" << s << ") error: " << err << " " << gluErrorString(err) << std::endl;
}

}
