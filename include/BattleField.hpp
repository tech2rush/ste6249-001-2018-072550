#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "SceneObject.hpp"
#include "glm/glm.hpp"

class BattleField : public SceneObject
{
	public:
		BattleField();
		~BattleField();

  protected:
    virtual void privateInit();
		virtual void privateRender();
		virtual void privateUpdate();

	private:
    std::vector< glm::vec3 > vertexArray_; // Maybe two-dim vector and several arrays
    // normal array.
    // texture coord array
};

